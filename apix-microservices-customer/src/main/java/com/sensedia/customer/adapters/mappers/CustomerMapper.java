package com.sensedia.customer.adapters.mappers;

import com.sensedia.customer.adapters.dtos.CustomerCreationDto;
import com.sensedia.customer.adapters.dtos.CustomerDto;
import com.sensedia.customer.domains.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
  Customer toCustomer(CustomerCreationDto customerCreationDto);

  CustomerDto toCustomerDto(Customer customer);

}
