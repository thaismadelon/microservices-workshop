package ${package}.commons.errors.resolvers;

import com.google.common.base.CaseFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value = {"classpath:ValidationMessages.properties"})
public abstract class BaseResolver {

  @Value("${${package}.InvalidField.message}")
  protected String invalidFieldMessage;

  @Value("${${package}.MissingField.message}")
  protected String missingFieldMessage;

  protected String convertToSnakeCase(String value) {
    return CaseFormat.LOWER_CAMEL.converterTo(CaseFormat.LOWER_UNDERSCORE).convert(value);
  }
}
